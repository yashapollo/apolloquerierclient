import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuerierComponent } from './querier/querier.component';

const routes: Routes = [
  { path: 'Querier', component: QuerierComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
