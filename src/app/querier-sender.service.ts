import { Injectable, APP_INITIALIZER } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient , HttpHeaders, HttpParams  } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class QuerierSenderService {
  private configUrl: 'localhost';

  constructor(private http: HttpClient) {
  }

  sendData(tableSelect: any, querySelect: any, time: any) {
    console.warn(tableSelect, querySelect, time);
    const params = new HttpParams()
      .set('tableName', tableSelect)
      .set('queryType', querySelect)
      .set('time', time);
    return this.http.post(this.configUrl, {params})
      .subscribe(
        data => console.log('success', data),
        error => console.log('oops', error)
      );
  }
}
