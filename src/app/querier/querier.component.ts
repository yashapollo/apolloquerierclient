import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {QueryRequest} from '../query-request';
import {QuerierSenderService} from '../querier-sender.service';
import {NgModule} from '@angular/core';


@Component({
  templateUrl: './querier.component.html',
  styleUrls: ['./querier.component.scss']
})
export class QuerierComponent implements OnInit {
  queryRequest: any;
  queryResult: any ;
  submitted = false;
  queryForm = new FormGroup({
    tableSelect: new FormControl('', Validators.required),
    querySelect: new FormControl('', Validators.required),
    time: new FormGroup({
      numTime: new FormControl('', Validators.required),
      typeMeasure: new FormControl('', Validators.required)
    })
  });
  tableNames = ['Loc', 'Dj'];
  queryTypes = ['timeInLeo', '40tsys', 'totalnum'];
  timeMeasurement = ['seconds', 'minutes', 'hours', 'days', 'weeks'];

  // tslint:disable-next-line: no-shadowed-variable
  constructor(private QuerierSenderService: QuerierSenderService) {
    this.queryRequest = new QueryRequest( ' ', ' ', 1, '');
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn('Your request has been submitted', this.queryRequest.toString());
    console.warn(this.queryForm.value);
    this.queryResult = this.QuerierSenderService.sendData(this.queryRequest.tableName, this.queryRequest.queryType, this.queryRequest.time);
    this.submitted = true;
    console.warn(this.queryResult);
    }

  ngOnInit(): void {
  }
}

