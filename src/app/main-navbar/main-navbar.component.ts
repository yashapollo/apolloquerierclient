import { Component, OnInit } from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef, OnDestroy} from '@angular/core';

@Component({
  selector: 'app-main-navbar',
  templateUrl: './main-navbar.component.html',
  styleUrls: ['./main-navbar.component.scss']
})
export class MainNavbarComponent implements OnInit {
  mobileQuery: MediaQueryList;
  optionArray = [' ', 'Querier', 'AutomaticChecks', 'Help', 'About'];
  fillerNav = Array.from({length: this.optionArray.length }, (_, i) => `${this.optionArray[i]}`);

constructor() {}
  ngOnInit() {}

}
