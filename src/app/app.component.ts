import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ApolloQuerier';
  envSelected: string;
  env = ['Apollo-drp', 'Apollo-scale-io', 'Apollo-exa', 'Apollo-trg', 'Apollo-integ' ];
}
