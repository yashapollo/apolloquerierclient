export class QueryRequest {
  constructor(
    public queryType: string,
    public tableName: string,
    public time: number,
    public timeMeasure: string,
    ) {  }
   toString(): string {
      return 'queryrequest:\n' + 'queryType:  ' + this.queryType + '\ntableName:  '
       + this.tableName + '\n time:  ' + this.time + ' ' + this.timeMeasure;
    }
}
