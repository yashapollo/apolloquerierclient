import { TestBed } from '@angular/core/testing';

import { QuerierSenderService } from './querier-sender.service';

describe('QuerierSenderService', () => {
  let service: QuerierSenderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuerierSenderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
